<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriaProducto extends Model
{
    protected $table = 'categoria_producto';

    protected $fillable = ['id', 'Nombre','Descripcion','Creador','Promocion'];

    public function subcategorias(){
        return $this->hasMany(SubCategoriaProducto::class,'categoria_id');
     }

}
