<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleUsuario extends Model
{
    protected $table = 'detalle_usuario';

    protected $fillable = ['id', 'Apellido','TipoDocumento','NumeroDocumento','FechaNacimiento','Sexo'];

    public function usuario(){
        return $this->belongsTo(User::class);
     }

}
