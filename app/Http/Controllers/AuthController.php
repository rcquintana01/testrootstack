<?php

namespace App\Http\Controllers;

use App\DetalleUsuario;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * Registro de usuario
     */
    public function signUp(Request $request)
    {


        $input = $request->all();


        $validator = Validator::make($input, [
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string'
        ],[
            'email.unique' => 'El campo Correo tiene que ser único',
            'email.required' => 'El campo es obligatorio',
            'password.required' => 'El campo es obligatorio',
            'name.required' => 'El campo es obligatorio',

        ]);

        if($validator->fails()){
            return ['error'=>$validator->errors()];
            // return $this->jsonValidatorFailedResponse($validator->errors());
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        $detalle= new DetalleUsuario();
         $detalle->usuario_id =   $user->id;
         $detalle->save();
        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }

    /**
     * Inicio de sesión y creación de token
     */
    public function login(Request $request)
    {

        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ]);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');

        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString()
        ]);
    }
    /**
     * Cierre de sesión (anular el token)
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Obtener el objeto User como json
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }


    public function detail($id)
    {
        $usuario = User::with('detalles')->find($id);
        return response()->json([
       'detalles_usuario'=>$usuario]);

    }

    public function editPersonalDetail(Request $request )
    {

        $detalle = DetalleUsuario::find( $request->data['detalles']['id']);

        $detalle->Apellido = $request->data['detalles']['apellido'];
        $detalle->TipoDocumento = $request->data['detalles']['tipoDocumento'];
        $detalle->NumeroDocumento = $request->data['detalles']['numeroDocumento'];
        $detalle->Sexo = $request->data['detalles']['sexo'];
        $detalle->FechaNacimiento = $request->data['detalles']['fechaNacimiento'];

        $detalle->save();


        return response()->json([
            'success' => 'Successfully created user!'
        ], 201);

    }

    public function changePass(Request $request)
    {




        $user = User::find($request->id);

        if(Hash::check($request->passwordOld,$user->password)) {

            $user->password = bcrypt($request->password);
            $user->save();
        } else {
            return response()->json([
                'error' => " No coinciden las contraseña"
            ], 201);
        }




        // $user = User::create([
        //     'name' => $request->name,
        //     'email' => $request->email,
        //     'password' => bcrypt($request->password)
        // ]);

        // $detalle= new DetalleUsuario();
        //  $detalle->usuario_id =   $user->id;
        //  $detalle->save();
        // return response()->json([
        //     'message' => 'Successfully created user!'
        // ], 201);
    }








}
