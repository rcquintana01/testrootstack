<?php

namespace App\Http\Controllers\CategoriaProducto;

use App\CategoriaProducto;
use App\Http\Controllers\Controller;
use App\SubCategoriaProducto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use PhpOffice\PhpSpreadsheet\Calculation\Category;

class CategoriaProductoController extends Controller
{
    public function index()
    {

        return view('categoriaProducto.categoria_producto');

    }

     public function listaCategoriaProducto(Request $request)
    {
        $categoria_producto = CategoriaProducto::orderBy('id','ASC')->paginate(10);
        return ['pagination'=>[
            'total' =>  $categoria_producto->total(),
            'current_page' =>  $categoria_producto->currentPage(),
            'per_page' =>  $categoria_producto->perPage(),
            'last_page' =>  $categoria_producto->lastPage(),
            'from' =>  $categoria_producto->firstItem(),
            'to' =>  $categoria_producto->lastItem(),
        ],
        'categoria_producto'=>$categoria_producto];
    }

    public function eliminarCategoriaProducto($id)
    {
        $categoria_producto = CategoriaProducto::find($id);
        $subCategorias = SubCategoriaProducto::where([['categoria_id', $id]])->get();
        $upload_path =public_path('storage').$categoria_producto->Imagen;
        $categoria_producto->delete();
        return $id;
        

    }

    public function adicionarCategoriaProducto(Request $request)
    {

    $input = $request->all();

    $validator = Validator::make($input, [
        'Nombre' => ['required', 'unique:categoria_producto'],
        'Descripcion' => ['required'],
    ],[
        'Nombre.required' => 'El campo es obligatorio',
        'Descripcion.required' => 'El campo es obligatorio',
        'Nombre.unique' => 'El campo Nombre tiene que ser único',
    ]);

    if($validator->fails()){
        return ['error'=>$validator->errors()];
    }

    $categoria_producto_nueva = new CategoriaProducto();
    $categoria_producto_nueva->Nombre = $request->Nombre;
    $categoria_producto_nueva->Descripcion = $request->Descripcion;
    $categoria_producto_nueva->Creador = auth()->user()->name;
    $categoria_producto_nueva->save();

    return ['categoria_producto'=>$categoria_producto_nueva];
    }

    public function editCategoriaProducto( $id)
    {
        $categoria_producto = CategoriaProducto::find($id);
        return ['categoria_producto'=>$categoria_producto];
    }

    public function updateCategoria(Request $request, $id){

        $categoria_producto = CategoriaProducto::find($id)->Nombre;
        $input = $request->all();
        if($categoria_producto==$request->Nombre){

            $validator = Validator::make($input, [
                'Nombre' => ['required'],
                'Descripcion' => ['required'],
            ],[
                'Nombre.required' => 'El campo es obligatorio',
                'Descripcion.required' => 'El campo es obligatorio'
            ]);
            if($validator->fails()){
                return ['error'=>$validator->errors()];
                
            }

        }
        else{
            $validator = Validator::make($input, [
                'Nombre' => ['required', 'unique:categoria_producto'],
                'Descripcion' => ['required'],
            ],[
                'Nombre.required' => 'El campo es obligatorio',
                'Descripcion.required' => 'El campo es obligatorio',
                'Nombre.unique' => 'El campo Nombre tiene que ser único',
            ]);
            if($validator->fails()){
                return ['error'=>$validator->errors()];
                
            }


        }

        if( $request->Promocion==true){

        $nombre = CategoriaProducto::where([['Promocion', '1']])->get('Nombre')->first();

        if($nombre && $nombre['Nombre'] != $categoria_producto){
            return ['errorPromocion'=>"Ya existe una Categoría para Promociones"];
        }
    }

        $categoria_producto = CategoriaProducto::find($id);
        $categoria_producto->Nombre = $request->Nombre;
        $categoria_producto->Descripcion = $request->Descripcion;
        if($request->Promocion==true){
            $categoria_producto->Promocion = 1;
        }
        else{
            $categoria_producto->Promocion = 0;

        }
        $categoria_producto->save();

        $categoria_producto = CategoriaProducto::all();
        return ['categoria_producto'=>$categoria_producto];

    }

    public function adicionarImagen(Request $request){


        
        $i=0;
        $file_name="";


        
        $idCategoria = CategoriaProducto::where('Nombre',$request['nombre'])->first()->id;

        $cantFotos=$request['cantFotos'];


        $upload_path =public_path('storage').'/Categorias/imagenes/'.$idCategoria;


        for ($i=0; $i < $cantFotos; $i++) {
            $time = time()+$i;

        $generated_new_name = $time  . '.' . $request['file'.$i]->getClientOriginalExtension();
        $request['file'.$i]->move($upload_path, $generated_new_name);

        //Insertando en la tabla que relaciona producto con imagenes
           $categoria = CategoriaProducto::find($idCategoria);
           $categoria->Imagen = '/Categorias/imagenes/'.$idCategoria.'/'.$time.'.'.$request['file'.$i]->getClientOriginalExtension();

           $categoria->save();
        }


    }

    public function editarImagen(Request $request){

        
        $i=0;
        $file_name="";

       
        $idCategoria = $request['id'];

        $categoria = CategoriaProducto::find($idCategoria);

        $cantFotos=$request['cantFotos'];


        $upload_path =public_path('storage').$categoria->Imagen;

        unlink($upload_path);


        $upload_path =public_path('storage').'/Categorias/imagenes/'.$idCategoria;


        for ($i=0; $i < $cantFotos; $i++) {
            $time = time()+$i;

        $generated_new_name = $time  . '.' . $request['file'.$i]->getClientOriginalExtension();
        $request['file'.$i]->move($upload_path, $generated_new_name);

        
        $categoria->Imagen = '/Categorias/imagenes/'.$idCategoria.'/'.$time.'.'.$request['file'.$i]->getClientOriginalExtension();

        $categoria->save();
        }


    }


}
