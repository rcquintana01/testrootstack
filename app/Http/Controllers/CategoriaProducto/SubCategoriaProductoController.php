<?php

namespace App\Http\Controllers\CategoriaProducto;
use App\CategoriaProducto;
use App\Http\Controllers\Controller;
use App\SubCategoriaProducto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SubCategoriaProductoController extends Controller
{
    public function index()
    {

        return view('categoriaProducto.sub_categoria_producto');

    }

     public function listaSubCategoriaProducto(Request $request)
    {
        $sub_categoria_producto = SubCategoriaProducto::with('categoria')->orderBy('id','ASC')->paginate(10);
        $categorias = CategoriaProducto::all();
        return ['pagination'=>[
            'total' =>  $sub_categoria_producto->total(),
            'current_page' =>  $sub_categoria_producto->currentPage(),
            'per_page' =>  $sub_categoria_producto->perPage(),
            'last_page' =>  $sub_categoria_producto->lastPage(),
            'from' =>  $sub_categoria_producto->firstItem(),
            'to' =>  $sub_categoria_producto->lastItem(),
        ],
        'sub_categoria_producto'=>$sub_categoria_producto,
        'categorias'=>$categorias];
    }

    public function eliminarSubCategoriaProducto($id)
    {


        $subcategoria_producto = SubCategoriaProducto::find($id);
        $subcategoria_producto->delete();
        return $id;

    }

    public function adicionarSubCategoriaProducto(Request $request)
    {

        $input = $request->all();

    $validator = Validator::make($input, [
        'Nombre' => ['required'],
        'Descripcion' => ['required'],
        'Categoria' => ['required'],
    ],[
        'Nombre.required' => 'El campo es obligatorio',
        'Descripcion.required' => 'El campo es obligatorio',
        'Categoria.required' => 'El campo es obligatorio',
    ]);

    if($validator->fails()){
        return ['error'=>$validator->errors()];
        // return $this->jsonValidatorFailedResponse($validator->errors());
    }

    $validarSub = SubCategoriaProducto::where([['Nombre',$request->Nombre],['categoria_id',$request->Categoria]])->get();


    if(count($validarSub)){
        return ['error'=>["Nombre"=>["El campo Nombre tiene que ser único para esa categoría"]]];
    }


    $sub_categoria_producto_nueva = new SubCategoriaProducto();
    $sub_categoria_producto_nueva->Nombre = $request->Nombre;
    $sub_categoria_producto_nueva->Descripcion = $request->Descripcion;
    $sub_categoria_producto_nueva->categoria_id = $request->Categoria;
    $sub_categoria_producto_nueva->Creador = auth()->user()->name;
    $sub_categoria_producto_nueva->save();

    return ['sub_categoria_producto_nueva'=>$sub_categoria_producto_nueva];
    }

    public function editSubCategoriaProducto( $id)
    {
        $sub_categoria_producto_nueva = SubCategoriaProducto::find($id);
        return ['sub_categoria_producto'=>$sub_categoria_producto_nueva];
    }

    public function updateSubCategoria(Request $request, $id){

        $sub_categoria_producto_nueva = SubCategoriaProducto::find($id)->Nombre;
        $input = $request->all();


            $validator = Validator::make($input, [
                'Nombre' => ['required'],
                'Descripcion' => ['required'],
                'Categoria' => ['required'],

            ],[
                'Nombre.required' => 'El campo es obligatorio',
                'Descripcion.required' => 'El campo es obligatorio',
                'Categoria.required' => 'El campo es obligatorio'
            ]);
            if($validator->fails()){
                return ['error'=>$validator->errors()];
               
            }



            $validarSub = SubCategoriaProducto::where([['Nombre',$request->Nombre],['categoria_id',$request->Categoria]])->get('id');
            


            if(count($validarSub) && $validarSub[0]['id']!=$id ){
                 return ['error'=>["Nombre"=>["El campo Nombre tiene que ser único para esa categoría"]]];

            }



        $sub_categoria_producto_nueva = SubCategoriaProducto::find($id);
        $sub_categoria_producto_nueva->Nombre = $request->Nombre;
        $sub_categoria_producto_nueva->Descripcion = $request->Descripcion;
        $sub_categoria_producto_nueva->categoria_id = $request->Categoria;
        $sub_categoria_producto_nueva->save();

        $sub_categoria_producto_nueva = SubCategoriaProducto::all();
        return ['sub_categoria_producto_nueva'=>$sub_categoria_producto_nueva];

    }

}
