<?php

namespace App\Http\Controllers\User;

use App\DetalleUsuario;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{


    public function __construct()
{
    $this->middleware('admin');
}


    public function index()
    {



        //$users = DB::table('users')->get();

        $users = User::all();


        $title = 'Listado de usuarios';

        //para imprimir
        //  dd(compact('title','users'));

        //Enviar valores a una vista con un arreglo asociativo automatico
        return view('users.index', compact('title', 'users'));

    }


    public function store()
    {

        $data = request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ], [
            'name.required' => 'El campo nombre es obligatorio',
            'email.required' => 'El campo correo es obligatorio',
            'email.unique' => 'El campo correo tiene que ser único',
            'password.required' => 'El campo contraseña es obligatorio',
            'password.confirmed' => 'Las contraseñas deben coincidir',
            'password.min' => 'Debe tener al menos 8 caracteres'
        ]);
        $is_admin = '0';
        if (request()->only('is_admin')) {
            $is_admin = '1';
        }
       $user= User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'is_admin' => $is_admin
        ]);

        $detalle= new DetalleUsuario();
        $detalle->usuario_id =   $user->id;
        $detalle->save();




        return redirect()->route('users_list');
    }

    public function edit( $id)
    {
        $user = User::find($id);
        return view('users.edit', ['user' => $user]);
    }

    public function update(User $user)
    {
        if (request()->only('password')['password'] == '') {
            $data = request()->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $user->id],

            ], [
                'name.required' => 'El campo nombre es obligatorio',
                'email.required' => 'El campo correo es obligatorio',
                'email.unique' => 'El campo correo tiene que ser único',
            ]);
            $data['password'] = $user->password;
        } else {
            $data = request()->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users,email,' . $user->id],
                'password' => ['string', 'min:8', 'confirmed'],
            ], [
                'name.required' => 'El campo nombre es obligatorio',
                'email.required' => 'El campo correo es obligatorio',
                'email.unique' => 'El campo correo tiene que ser único',
                'password.confirmed' => 'Las contraseñas deben coincidir',
                'password.min' => 'Debe tener al menos 8 caracteres'
            ]);
            $data['password'] = bcrypt(request()->only('password')['password']);
        }

        $data['is_admin']='0';
        if (request()->only('is_admin')) {
            $data['is_admin']='1';
        }

        $user->update($data);

        return redirect()->route('users_list');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
         return $id;
        //  return redirect()->route('users_list');
    }

     public function userList()
    {

       return User::all();
    }



}
