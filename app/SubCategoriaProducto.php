<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategoriaProducto extends Model
{
    protected $table = 'sub_categoria_producto';

    protected $fillable = ['id', 'Nombre','Descripcion','Creador','categoria_id'];

    public function categoria(){
        return $this->belongsTo(CategoriaProducto::class);
    }

}
