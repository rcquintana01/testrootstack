<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetalleUsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_usuario', function (Blueprint $table) {
            $table->id();
            $table->string('Apellido', 80)->nullable();
            $table->string('TipoDocumento', 4)->nullable();
            $table->string('NumeroDocumento', 15)->nullable();
            $table->date('FechaNacimiento')->nullable();
            $table->string('Sexo',10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalle_usuario');
    }
}
