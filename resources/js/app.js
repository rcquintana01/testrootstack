/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import VeeValidate from "vee-validate";
import localize from 'vee-validate';

// Import component
import Loading from 'vue-loading-overlay';
// Import stylesheet
import 'vue-loading-overlay/dist/vue-loading.css';






import BootstrapVue, { BootstrapVueIcons } from "bootstrap-vue";

import { CChartBar, CChartLine, CChartHorizontalBar } from '@coreui/vue-chartjs';

import KProgress from 'k-progress';



import Calendar from 'v-calendar/lib/components/calendar.umd';
import DatePicker from 'v-calendar/lib/components/date-picker.umd';

import { Line, mixins, Bar } from 'vue-chartjs'



import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import Multiselect from 'vue-multiselect'


window.Vue = require('vue');


Vue.component('k-progress', KProgress);


// Register components in your 'main.js'
Vue.component('calendar', Calendar);
Vue.component('v-date-picker', DatePicker);



/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('Ejemplo-component', require('./components/Ejemplo.vue').default);

Vue.component('card-component', require('./components/CardComponent.vue').default);
Vue.component('categoria_producto-component', require('./components/CategoriaProducto/CategoriaProducto.vue').default);
Vue.component('sub_categoria_producto-component', require('./components/CategoriaProducto/SubCategoriaProducto.vue').default);


























Vue.component('user-component', require('./components/user/UserComponent.vue').default);


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */


Vue.component('CChartBar', CChartBar)
Vue.component('CChartLine', CChartLine)









Vue.filter('capitalize', function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
})

Vue.filter('formatDate', function (value) {
    if (value) {
        return moment(String(value)).format('DD/MM/YYYY hh:mm:ss')
    }
})

Vue.use(VeeValidate, {
    inject: true,
    fieldsBagName: "veeFields",
    errorBagName: "veeErrors",
    classes: true, // Sí queremos que ponga clases automáticamente
    // Indicar qué clase poner al input en caso de que sea
    // válido o inválido. Usamos Bootstrap 4
    classNames: {
        valid: "is-valid",
        invalid: "is-invalid",
    },
});

// VeeValidate.Validator.localize("es");

VeeValidate.Validator.extend('validaCI', {
    getMessage: (campo, argumentos) => {
        // argumentos es un arreglo
        if (argumentos == 1)
            return "El campo debe tener 10 caracteres ";
        else
            return "El campo debe tener menos de 12 caracteres ";
    },
    validate: (valor, argumentos) => {
        if (argumentos == 1)
            return valor.length == 10 ? true : false;
        else
            return valor.length <= 12 ? true : false;
    },

})

VeeValidate.Validator.extend('PlazoXCreditoMaximo', {
    getMessage: (campo, argumentos) => {
        if (argumentos == 1 || argumentos == 3) {
            return "El plazo máx. para el TC es 60";
        } else {
            return "El plazo máx. para el TC es 48";
        }
    },
    validate: (valor, argumentos) => {
        if (argumentos == 1) {
            if (valor > 60)
                return false;
            else
                return true;
        } else if (argumentos == 2) {
            if (valor > 48)
                return false;
            else
                return true;
        } else {
            if (valor > 60)
                return false;
            else
                return true;
        }
    },

})

VeeValidate.Validator.extend('PlazoXCreditoMinimo', {
    getMessage: (campo, argumentos) => {

        if (argumentos == 1 || argumentos == 2) {
            return "El plazo mín. para el TC es 12";
        } else {
            return "El plazo mín. para el TC es 36";
        }
    },
    validate: (valor, argumentos) => {
        if (argumentos == 1 || argumentos == 2) {
            if (valor < 12)
                return false;
            else
                return true;
        } else {
            if (valor < 36)
                return false;
            else
                return true;
        }
    },

})




VeeValidate.Validator.extend('numerosDecimales', {
    getMessage: (campo) => {

        return "El campo requiere un número decimal mayor que 0";
    },
    validate: (valor) => {
        var expreg = /^[.]?[0-9]+[.]?[0-9]*$/;

        var val = valor.toString();
        if (val.charAt(val.length - 1) == '.' || valor <= 0) {
            return false;
        }


        if (expreg.test(valor))
            return true;
        else
            return false;
    },
})

VeeValidate.Validator.extend('validaTasa', {
    getMessage: (campo) => {

        return "El valor máximo permitido es 16.06";
    },
    validate: (valor) => {

        if (valor > 16.06) {
            return false;
        } else
            return true;
    },
})

VeeValidate.Validator.extend('repetido', {
    getMessage: (campo, argumentos) => {
        // argumentos es un arreglo
        return "Esta " + campo + " ya está registrada";
    },
    validate: (valor, argumentos) => {
        if (argumentos != '')
            for (let i = 0; i < argumentos.length; i++) {
                if (argumentos[i].Nombre == valor) {
                    return false;
                }

            }
        return true;
    },

})

VeeValidate.Validator.extend('repetido2', {
    getMessage: (campo, argumentos) => {
        // argumentos es un arreglo
        return "Esta " + campo + " ya está registrada";
    },
    validate: (valor, argumentos) => {
        if (argumentos != '') {
            for (let i = 0; i < argumentos.length; i++) {
                if (argumentos[i].Nombre == valor) {
                    return false;
                }

            }
        }


        return true;
    },

})

VeeValidate.Validator.extend('repetidoConcesionario', {
    getMessage: (campo, argumentos) => {
        // argumentos es un arreglo
        return "Este " + campo + " ya está registrado";
    },
    validate: (valor, argumentos) => {
        for (let i = 0; i < argumentos.length; i++) {

            if (argumentos[i].Nombre == valor) {

                return false;
            }

        }
        return true;
    },

})


VeeValidate.Validator.localize({
    en: {
        messages: {
            required: 'El campo es obligatorio',
            integer: 'El campo solo admite valores numéricos enteros',
            alpha_num: 'El campo solo admites letras o números'
        },
        fields: {
            password: {
                required: 'Password cannot be empty!',
                max: 'Are you really going to remember that?',
                min: 'Too few, you want to get doxed?'
            }
        }
    }
});



Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons);

const app = new Vue({
    el: '#app',
    data: {
        showModal: false
    }
});