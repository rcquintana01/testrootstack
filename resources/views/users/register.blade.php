@extends('admin.index')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card mx-4">
                    <div class="card-body p-4">
                        <h1>Registrar Usuario</h1>
                        <p class="text-muted"></p>

                    <form method="POST" action="{{ route('user_new') }}">
                            @csrf
                        <div class="input-group mb-3">
                            <div class="input-group-prepend"> <span class="input-group-text">
                                    <i class="icon-user"></i>
                                </span>
                            </div>
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                        name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Nombre">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend"> <span class="input-group-text">
                                    <i class="icon-envelope-open"></i>
                                </span></div>
                            <input id="email" type="email" placeholder="Correo" class="form-control @error('email') is-invalid @enderror"
                                        name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend"> <span class="input-group-text">
                                    <i class="icon-lock"></i>
                                </span></div>
                            <input id="password" placeholder="Contraseña" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        required autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                        </div>
                        <div class="input-group mb-4">
                            <div class="input-group-prepend"> <span class="input-group-text">
                                    <i class="icon-lock"></i>
                                </span></div>
                            <input id="password-confirm" type="password" class="form-control"
                                        name="password_confirmation" required autocomplete="new-password" placeholder="Repetir Contraseña">


                        </div>
                        <div class="input-group mb-4">

                            <div class="form-check" style="margin-left: 1%">
                                <input class="form-check-input" type="checkbox" name="is_admin" id="is_admin">

                                <label class="form-check-label" for="remember">
                                    Administrador
                                </label>
                            </div>
                        </div>
                        <button class="btn btn-block btn-success" type="submit">Crear Usuario</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
