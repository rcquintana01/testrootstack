<?php

use App\Exports\CargaMasivaExport;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return redirect('/login');
});
Route::get('admin/layout', function () {
    return view('admin.login');
});
Route::get('admin/index', function () {
    return view('admin.index');
});

Route::get('/users', function () {
    return view('users.index')->middleware('auth');
});
Route::get('/users/register', function () {
    return view('users.register');
})->name('users.register')->middleware('auth');


//Rutas de usuarios
//Devuelve todos los usuarios por la llamada desde vue
Route::get('/users_list', 'User\UserController@index')->name('users_list');
//Ruta para registrar usaurios
Route::post('/user/new', 'User\UserController@store')->name('user_new');

Route::get('/users/{user}/edit', 'User\UserController@edit')->name('users.edit');

Route::put('/users/{user}', 'User\UserController@update')->name('users.update');

Route::delete('/users/{id}', 'User\UserController@destroy')->name('users.destroy');

Route::get('/userList', 'User\UserController@userList');

Route::get('/userName', 'HomeController@userName');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');


// Categoria de producto
Route::get('/categoriaProducto', 'CategoriaProducto\CategoriaProductoController@index')->middleware('auth');
Route::get('/categoriaProductoList', 'CategoriaProducto\CategoriaProductoController@listaCategoriaProducto')->middleware('auth');
Route::post('/categoriaProducto/new', 'CategoriaProducto\CategoriaProductoController@adicionarCategoriaProducto')->name('adicionarCategoriaProducto')->middleware('auth');
Route::delete('/categoriaProducto/{id}', 'CategoriaProducto\CategoriaProductoController@eliminarCategoriaProducto')->name('eliminarCategoriaProducto')->middleware('auth');
Route::post('/categoriaProducto/edit/{id}', 'CategoriaProducto\CategoriaProductoController@editCategoriaProducto')->name('editarCategoriaProducto')->middleware('auth');
Route::post('/categoriaProducto/{id}', 'CategoriaProducto\CategoriaProductoController@updateCategoria')->name('categoria.update')->middleware('auth');

//  Subcategoria de producto
Route::get('/subcategoriaProducto', 'CategoriaProducto\SubCategoriaProductoController@index')->middleware('auth');
Route::get('/subcategoriaProductoList', 'CategoriaProducto\SubCategoriaProductoController@listaSubCategoriaProducto')->middleware('auth');
Route::post('/subcategoriaProducto/new', 'CategoriaProducto\SubCategoriaProductoController@adicionarSubCategoriaProducto')->name('adicionarSubCategoriaProducto')->middleware('auth');
Route::delete('/subcategoriaProducto/{id}', 'CategoriaProducto\SubCategoriaProductoController@eliminarSubCategoriaProducto')->name('eliminarSubCategoriaProducto')->middleware('auth');
Route::post('/subcategoriaProducto/edit/{id}', 'CategoriaProducto\SubCategoriaProductoController@editSubCategoriaProducto')->name('editarSubCategoriaProducto')->middleware('auth');
Route::post('/subcategoriaProducto/{id}', 'CategoriaProducto\SubCategoriaProductoController@updateSubCategoria')->name('subcategoria.update')->middleware('auth');


//Scraping
Route::get('/scraping', 'ExternalScraping@index')->middleware('auth');;
